# Google Adsense Monitor

Google Adsense Monitor is an Opera extension that lets you view updates to your latest adsense earnings in an Opera Speed Dial.
	
	Download: https://addons.opera.com/extensions/details/google-adsense-monitor/
	Website: http://adsensemonitor.tumblr.com/
	Source: https://github.com/thewebdev/opera-extension-am.git
	Email: thewebdev@myopera.com

**Note**: This version of Google Adsense Monitor is for Opera 12.16 (Presto engine) or below only.  

## Features  

1. **Safe and Secure**  
The extension doesn't need to know your sign in email or password, and doesn't involve itself with the login process. It also doesn't store your sign in details anywhere nor does it share your Adsense data with any other website.

2. **Earnings in your Currency**  
Google Adsense Monitor can automatically convert and display your earnings in your local currency, if your Adsense report currency is either US dollars or Euro. (Warning: this is an experimental feature and may not always work or be error free).

3. **Simple, Effective Design**  
The colour of the earning data indicate whether your earnings have improved or not. Red indicates a drop in earning, while green indicates an improvement.

4. **Automatic Updates**  
Once you have logged in, Google Adsense Monitor downloads new Adsense earnings data every 15 minutes.  

5. **Customisable**  
Change display timings, data update intervals, options to view your earnings in different ways etc., as per your preference.   

## Usage

1. After logging into Google Adsense, don't sign out - just close the tab. Please note that it will take at least 2 minutes, after sign in, for the data to be updated and displayed in the Speed Dial.

2. The upward and the downward swing (indicated by the green and red colour, respectively) of the earnings data is based on the comparison with the previous period's earning (i.e. today's earning is compared with yesterday's, and this month's earning is compared with last month's).

## Changelog

Version 12.10 -  
Fixed: Revised versioning. (See Issue #10 ).  

Version 12.01 -  
New: (Experimental) Automatically converts earnings into local currency from USD or EUR.  
New: Versioning process.  
Fixed: Issue #7.  

Version 1.8 -  
New: Display all earnings data in one view.  
New: Options to control display & update timings.  

Version 1.6 -  
New: Extension icon for Mac / Retina displays.  
Updated: Copyright year to 2013.  
Updated: Extension 'promo tag' in preferences.  

Version 1.5 -  
New: Preferences allows user to select the earnings data he / she wants to view.  

Version 1.2 -  
Display design updated to provide more context.

Version 1.1 -  
New: Displays 'Total Unpaid Earnings' too.
